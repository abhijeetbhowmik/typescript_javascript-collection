
export interface ICounterOptions {
  start?: number;
  delta?: ((prev_delta?: number) => number);
}

export class Counter {
  private count: number;
  private start: number;
  private delta: ((prev_delta?: number) => number);

  constructor(options: ICounterOptions) {
    Object.assign(this, options);
    this.count = this.start;
    this.delta = (options.delta).bind(this);
  }
  /**
   * 
   * @returns next count
   */
  
  public tick() {
    // Complete the function to meet following requirements:
    // Counter should move the count as per delta function
    // Counter should return the new count value
  }

  public reset() {
    // reset the count
  }
}

// driver code below

// fix the code so that following works
let incrementCounter = new Counter({
  start: 0,
  delta: 1
})

// fix the code so that following works
let decreementCounter = new Counter({
  start: 0,
  delta: -1
})

let incrementCustomCounter = new Counter({
  start: 0,
  delta: (curr_count) => {
    // change the delta to return count^2
    return curr_count > 0 ? curr_count*2 : 1
  }
})

// following doesn't work. Please fix it
let decreementCustomCounter = new Counter({
  start: 0,
  delta: (curr_count) => {
    return curr_count*-2
  }
})
